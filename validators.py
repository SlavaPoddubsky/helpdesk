import json
from datetime import datetime


class ValidationError(Exception):

    def __init__(self, message):
        self.message = message


class BaseValidator(object):

    VALID_ATTRIBUTES = []
    REQUIRED_ATTRIBUTES = []

    def __init__(self, data=None):
        self._errors = {}
        self._input_data = data if isinstance(data, dict) else None
        self._clean_data = {}

    def get_errors(self):
        return self._errors

    def get_clean_data(self):
        return self._clean_data

    def is_valid(self, *args):
        for field, value in self._input_data.items():
            if field not in self.VALID_ATTRIBUTES:
                self._errors[field] = 'Unexpected field "{}".'.format(field)
        for field in self.REQUIRED_ATTRIBUTES:
            if not self._input_data.get(field, ''):
                self._errors[field] = 'Required field "{}" is not presented.'.format(field)
        if self._errors:
            return False
        for field, value in self._input_data.items():
            try:
                clean_method = getattr(self, "clean_{}".format(field))
                if not clean_method:
                    raise NotImplementedError(
                        "Clean method for field {} is not implemented.".format(field)
                    )
                self._clean_data[field] = clean_method(value)
            except ValidationError as ex:
                self._errors[field] = ex.message
        return True if not self._errors else False


class MessageValidator(BaseValidator):

    VALID_ATTRIBUTES = ['client_id', 'user_name', 'message', 'created_at']
    REQUIRED_ATTRIBUTES = ['message']
    MESSAGES_PER_SECOND_LIMIT = 10
    MESSAGE_MAX_LENGTH = 1000
    USERNAME_MAX_LENGTH = 200

    _messages_per_second = {}

    def __init__(self, *args, **kwargs):
        self._available_clients = []
        super(MessageValidator, self).__init__(*args, **kwargs)

    def is_valid(self, json_message, ip, client_id=None, available_clients=None):
        self._check_messages_limit(ip)
        if self._errors:
            return False

        self._available_clients = available_clients
        try:
            self._input_data = json.loads(json_message)
            if client_id:
                self._input_data['client_id'] = client_id
        except ValueError:
            self._errors['format'] = 'Invalid input format.'
            return False
        return super(MessageValidator, self).is_valid()

    def _check_messages_limit(self, ip):
        current_second = int(datetime.now().timestamp())
        last_message_second, amount = self._messages_per_second.get(ip, (current_second, 0))
        amount += 1
        if current_second == last_message_second:
            if amount > self.MESSAGES_PER_SECOND_LIMIT:
                self._errors['message'] = "Limit per second has been exceeded."
        else:
            amount = 1
        self._messages_per_second[ip] = (current_second, amount)

    def clean_client_id(self, client_id):
        if not self._available_clients:
            return client_id
        if not client_id or client_id not in self._available_clients:
            raise ValidationError("Such client is not exists.")
        return client_id

    def clean_message(self, message):
        if not message or len(message) > self.MESSAGE_MAX_LENGTH:
            raise ValidationError(
                "Message must be not empty "
                "and has less than {} symbols.".format(self.MESSAGE_MAX_LENGTH)
            )
        return message.strip()

    def clean_user_name(self, user_name):
        if user_name and len(user_name) > self.USERNAME_MAX_LENGTH:
            raise ValidationError(
                "User name must has less than {} symbols.".format(self.USERNAME_MAX_LENGTH)
            )
        return user_name.strip() if user_name else ''

    def clean_created_at(self, created_at):
        if not created_at:
            created_at = datetime.now().timestamp()
        try:
            datetime.fromtimestamp(created_at)
        except ValueError as ex:
            raise ValidationError(str(ex))
        return created_at