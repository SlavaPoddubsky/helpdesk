import tornado.ioloop
import tornado.web
import tornado.websocket

from handlers import *
from settings import TORNADO_SETTINGS


application = tornado.web.Application([
    (r"/", ClientHandler),
    (r"/admin", AdminHandler),
    (r"/admin/messages/(.*)", MessagesHandler),
    (r"/ws/client/", ClientWSHandler),
    (r"/ws/admin/", AdminWSHandler),
], **TORNADO_SETTINGS)

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()