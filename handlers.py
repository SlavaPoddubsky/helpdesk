import json
import uuid
from datetime import datetime
import tornado.web
import tornado.websocket
from mappers import ClientsMapper, ClientNamesMapper, MessagesMapper, ConversationMapper
from validators import MessageValidator


class ClientHandler(tornado.web.RequestHandler):

    def get(self):
        """
        Specify client id via cookies, fetch last name and
        messages history from Redis DB and show page
        """
        client_id = self.get_cookie('client_id', None)
        if not client_id:
            client_id = str(uuid.uuid4())
            self.set_cookie('client_id', client_id)
            clients = ClientsMapper()
            clients.add_client(client_id)
        client_names = ClientNamesMapper()
        messages = MessagesMapper()
        self.render('client.html', **{
            'ws_url': "ws://{}/ws/client/".format(self.request.host),
            'user_name': client_names.get_last_name(client_id),
            'messages': messages.get_messages(client_id),
            'is_admin_online': AdminWSHandler.is_online()
        })


class AdminHandler(tornado.web.RequestHandler):

    def get(self):
        """
        Show page with list of conversations, that are loaded from Redis DB
        """
        conversations = ConversationMapper().get_conversations_list()
        for item in conversations:
            item['is_online'] = item['client_id'] in ClientWSHandler.clients.keys()
        self.render('admin.html', **{
            'ws_url': "ws://{}/ws/admin/".format(self.request.host),
            'conversations': conversations,
        })


class MessagesHandler(tornado.web.RequestHandler):

    def get(self, client_id=None):
        """
        Show list of client's messages as JSON
        """
        messages = MessagesMapper()
        messages_list = messages.get_messages(client_id, for_json=True)
        self.write({'messages': messages_list})


class ClientWSHandler(tornado.websocket.WebSocketHandler):

    clients = {}

    def get_self_uuid(self):
        """
        Get client uuid from list of clients
        """
        for uuid, client in self.clients.items():
            if self is client:
                return uuid
        return None

    def update_status(self, client_id, is_online):
        """
        Send message to admin about client status changing

        :param client_id:       Unique client id
        :param is_online:       Boolean online status
        """
        admin = AdminWSHandler.get_instance()
        if admin:
            conversation_item = ConversationMapper().get_conversation_for(client_id)
            conversation_item['is_online'] = is_online
            admin.write_message(conversation_item)

    def send_ws_error(self, errors, error_type='ValidationError'):
        """
        Send error data back to client

        :param errors:dict      Dictionary with errors
        :param error_type:str   Type of error
        """
        self.write_message({'error': {
            'type': error_type,
            'details': errors
        }})

    def open(self):
        """
        When connection with client is opened,
        save it in clients' dictionary by client_id from cookies
        """
        client_id = self.get_cookie('client_id', None)
        self.clients[client_id] = self
        self.update_status(client_id, True)
        print("Client {} connected.".format(client_id))

    def on_message(self, json_message):
        """
        On message from client, send it to admin and save in Redis DB

        :param json_message:    Special request from client side in JSON format
        """
        validator = MessageValidator()
        client_id = self.get_self_uuid()
        ip = self.request.remote_ip
        if not validator.is_valid(json_message, ip, client_id):
            self.send_ws_error(validator.get_errors())
            return
        message_dict = validator.get_clean_data()
        user_name = message_dict.get('user_name')
        message = message_dict.get('message')
        created_at = datetime.fromtimestamp(message_dict.get('created_at'))

        admin = AdminWSHandler.get_instance()
        if admin:
            admin.write_message(message_dict)
        if user_name:
            ClientNamesMapper().set_name(client_id, user_name)

        message_dict = MessagesMapper().add_message(client_id, message, user_name, created_at)
        ConversationMapper().set_conversation(message_dict)
        self.update_status(client_id, True)

        print("{}: Client {} wrote message to admin: {}".format(
            created_at.strftime("%A, %d. %B %Y %I:%M%p"), client_id, message
        ))

    def on_close(self):
        """
        On close connection, remove client from
        available clients list and send status to admin
        """
        client_id = self.get_self_uuid()
        if client_id in self.clients.values():
            self.clients.pop(client_id)
            self.update_status(client_id, False)
        print("Client {} disconnected.".format(client_id))


class AdminWSHandler(tornado.websocket.WebSocketHandler):

    _is_online = False
    _instance = None

    @classmethod
    def is_online(cls):
        return cls._is_online

    @classmethod
    def get_instance(cls):
        return cls._instance

    def open(self):
        """
        When connection with admin is opened,
        save its instance in static field and alert
        all clients about online admin status
        """
        self.__class__._is_online = True
        self.__class__._instance = self
        print("Admin connected.")
        for client in ClientWSHandler.clients.values():
            client.write_message({'is_admin_online': True})

    def on_message(self, json_message):
        """
        On message from admin, send it to appropriate client
        and save message in Redis DB

        :param json_message:    Special request from client side in JSON format
        """
        validator = MessageValidator()
        ip = self.request.remote_ip
        available_clients = ClientWSHandler.clients.keys()

        if not validator.is_valid(json_message, ip, available_clients=available_clients):
            self.send_ws_error(validator.get_errors())
            return
        message_dict = validator.get_clean_data()
        client_id = message_dict.get('client_id')
        user_name = message_dict.get('user_name') or 'Admin'
        created_at = datetime.fromtimestamp(message_dict.get(
            'created_at', datetime.now().timestamp()
        ))
        message = message_dict.get('message')

        client = ClientWSHandler.clients.get(client_id)
        if client:
            client.write_message(message_dict)

        message_dict = MessagesMapper().add_message(client_id, message, user_name, created_at)
        ConversationMapper().set_conversation(message_dict)

        print("Admin wrote to {} next message: {}".format(client_id, message))

    def on_close(self):
        """
        On close connection, clean admin instance and alert
        all clients about offline admin status
        """
        self.__class__._is_online = False
        self.__class__._instance = None
        print("Admin disconnected.")
        for client in ClientWSHandler.clients.values():
            client.write_message({'is_admin_online': False})

    def send_ws_error(self, errors, error_type='ValidationError'):
        """
        Send error data back to client

        :param errors:dict      Dictionary with errors
        :param error_type:str   Type of error
        """
        self.write_message({'error': {
            'type': error_type,
            'details': errors
        }})