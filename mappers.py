from datetime import datetime
import redis
from settings import REDIS_SETTINGS


class MapperBase(object):
    def __init__(self, db=0):
        settings = REDIS_SETTINGS
        settings['db'] = db
        self.rd = redis.StrictRedis(**settings)


class ClientsMapper(MapperBase):
    @staticmethod
    def get_key():
        return "clients:sorted_set"

    def get_clients(self, start=0, limit=None):
        key = self.get_key()
        end = start + limit if limit else -1
        return [k.decode('utf-8') for k in self.rd.zrange(key, start, end)]

    def add_client(self, client_id, created_at=None):
        created_at = created_at or datetime.now()
        score = created_at.timestamp() \
            if isinstance(created_at, datetime) else created_at
        self.rd.zadd(self.get_key(), score, client_id)


class ClientNamesMapper(MapperBase):
    @staticmethod
    def get_key(client_id):
        return "client_names:sorted_set:{}".format(client_id)

    def get_last_name(self, client_id):
        key = self.get_key(client_id)
        names = self.rd.zrevrange(key, 0, 0)
        return names[0] if names else ''

    def set_name(self, client_id, name, created_at=None):
        created_at = created_at or datetime.now()
        score = created_at.timestamp() \
            if isinstance(created_at, datetime) else created_at
        self.rd.zadd(self.get_key(client_id), score, name)


class MessagesMapper(MapperBase):
    MESSAGE_FIELDS = ['client_id', 'message', 'created_at', 'user_name']

    @staticmethod
    def get_key(client_id, created_at):
        return "messages:dict:{}:{}".format(client_id, created_at)

    def add_message(self, client_id, message,
                    user_name=None, created_at=None):
        if not client_id:
            raise ValueError('client_id parameter is required.')
        if not message:
            raise ValueError('message parameter is required.')

        created_at = datetime.now().timestamp() \
            if not created_at else created_at.timestamp()
        message_dict = {
            'client_id': client_id,
            'message': message,
            'user_name': user_name or '',
            'created_at': created_at
        }
        key = self.get_key(client_id, created_at)
        for field in self.MESSAGE_FIELDS:
            self.rd.hset(key, field, message_dict.get(field))
        return message_dict

    def get_messages_count(self, client_id):
        key_pattern = self.get_key(client_id, '*')
        return len(self.rd.keys(key_pattern))

    def get_messages(self, client_id, for_json=False):
        key_pattern = self.get_key(client_id, '*')
        keys_list = self.rd.keys(key_pattern)
        messages_list = []
        for key in keys_list:
            message_item = {k.decode("utf-8"): v.decode("utf-8")
                            for k, v in self.rd.hgetall(key).items()}
            if not for_json:
                created_at = float(message_item.get('created_at'))
                message_item['created_at'] = datetime.fromtimestamp(created_at)
            messages_list.append(message_item)
        return sorted(messages_list, key=lambda k: k['created_at'])


class ConversationMapper(MapperBase):
    CONVERSATION_FIELDS = ['client_id', 'last_name', 'last_message', 'messages_count']

    @staticmethod
    def get_key(client_id):
        return "conversations:dict:{}".format(client_id)

    def set_conversation(self, message_dict):
        client_id = message_dict.get('client_id')
        key = self.get_key(client_id)
        if message_dict.get('last_name') == 'Admin':
            conversation_dict = {
                'messages_count': MessagesMapper().get_messages_count(client_id)
            }
        else:
            conversation_dict = {
                'client_id': client_id,
                'last_name': message_dict.get('user_name'),
                'last_message': message_dict.get('message'),
                'messages_count': MessagesMapper().get_messages_count(client_id)
            }

        for field in self.CONVERSATION_FIELDS:
            self.rd.hset(key, field, conversation_dict.get(field))

    def get_conversation_for(self, client_id):
        key = self.get_key(client_id)
        data = self.rd.hgetall(key)
        conversation_item = {
            'client_id': client_id,
            'last_name': data.get(b'last_name', b'').decode("utf-8"),
            'last_message': data.get(b'last_message', b'').decode("utf-8"),
            'messages_count': data.get(b'messages_count', b'0').decode("utf-8"),
        }
        return conversation_item

    def get_conversations_list(self):
        clients = ClientsMapper().get_clients()
        conversation_list = []
        for client_id in clients:
            conversation_item = self.get_conversation_for(client_id)
            conversation_list.append(conversation_item)
        return conversation_list