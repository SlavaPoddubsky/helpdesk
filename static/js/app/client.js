HelpDescClient = function(){
    this.ws = null;
    this.isAdminOnline = appConfig.isAdminOnline;
    this.$output = $('#messagesOutput')
};

HelpDescClient.RESTORE_CONNECTION_TIMEOUT = 1000;

HelpDescClient.prototype.onClose = function(){
    var self = this,
        restoreConnectionHandler = setInterval(function(){
        try {
            console.log('Restore connection...');
            self.getConnection(true);
            clearInterval(restoreConnectionHandler)
            console.log('Connection established.');
        } catch (ex) {
            console.log(ex)
        }
    }, HelpDescClient.RESTORE_CONNECTION_TIMEOUT);
};

HelpDescClient.prototype.onMessage = function(wsMessage){
    var msgObj = $.parseJSON(wsMessage.data);
    if(msgObj.error){
        console.log(msgObj.error.details)
    }
    if(msgObj.message) {
        this.addMessage(msgObj)
    }
    if(msgObj.is_admin_online !== undefined) {
        this.isAdminOnline = msgObj.is_admin_online;
        this.changeAdminStatus();
    }
};

HelpDescClient.prototype.changeAdminStatus = function(){
    var $alerts = $('#isAdminOnlineAlerts .alert');
    $alerts.hide();
    if(this.isAdminOnline){
        $alerts.filter('.alert-success').show();
    } else {
        $alerts.filter('.alert-danger').show();
    }
};

HelpDescClient.prototype.addMessage = function(msgObj){
    var createdAtDate = new Date(msgObj.created_at * 1000);
    var userName = msgObj.user_name || 'Anonymous',
        output = "<p>[" + createdAtDate.toLocaleString() + "] <strong>" +
            userName + "</strong>: " + msgObj.message + "</p>";
    this.$output.append(output)
};

HelpDescClient.prototype.getConnection = function(restore){
    var self = this;
    if(!this.ws || restore){
        this.ws = new WebSocket(appConfig.wsUrl);
        this.ws.onmessage = function(wsMessage){
            self.onMessage(wsMessage)
        };
        this.ws.onclose = function(){
            self.onClose()
        };
    }
    return this.ws
};

HelpDescClient.prototype.post = function(msgObj){
    this.getConnection().send(JSON.stringify(msgObj))
    this.addMessage(msgObj)
};

HelpDescClient.prototype.init = function(){
    this.changeAdminStatus();
    this.bindings();
    this.getConnection();
};

HelpDescClient.prototype.bindings = function(){
    var self = this;
    if(!HelpDescClient.isBinded){

        $('#sendMessagesForm').on('submit', function(){
            var $input = $('#messageInput');
            if($input.val()) {
                self.post({
                    message: $input.val(),
                    user_name: $('#userNameInput').val(),
                    created_at: new Date().getTime() / 1000
                });
                $input.val('').trigger('change');
            }
            return false;
        });

        $('#messageInput').on('keyup change', function(){
            $('#sendMessagesForm button').prop('disabled', $(this).val() == '')
        }).trigger('change');
    }
    HelpDescClient.isBinded = true

};

$(document).on('ready', function(){
    var client = new HelpDescClient();
    client.init();
});