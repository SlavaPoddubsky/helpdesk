TORNADO_SETTINGS = {
    'debug': True,
    'autoreload': True,
    'template_path': 'templates',
    'static_path': 'static'
}

REDIS_SETTINGS = {
    'host': 'localhost',
    'port': 6379
}