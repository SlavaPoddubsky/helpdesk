HelpDescAdmin = function(){
    this.ws = null;
    this.msgCompiledTmpl = _.template($('#messageItemTemplate').text());
    this.statusItemTmpl = _.template($('#conversationItemTemplate').text());
    this.$output = $('#messagesOutput');
    this.$clientsList = $('#conversationsList');
};

HelpDescAdmin.RESTORE_CONNECTION_TIMEOUT = 1000;

HelpDescAdmin.prototype.onClose = function(){
    var self = this,
        restoreConnectionHandler = setInterval(function(){
        try {
            console.log('Restore connection...');
            self.getConnection(true);
            clearInterval(restoreConnectionHandler)
            console.log('Connection established.');
        } catch (ex) {
            console.log(ex)
        }
    }, HelpDescAdmin.RESTORE_CONNECTION_TIMEOUT);
};

HelpDescAdmin.prototype.onMessage = function(wsMessage){
    console.log(wsMessage);
    var msgObj = $.parseJSON(wsMessage.data);
    if(msgObj.error){
        console.log(msgObj.error.details)
    }
    if(msgObj.message) {
        this.addMessage(msgObj);
    }
    if(msgObj.is_online !== undefined){
        this.updateConversation(msgObj);
    }
    this.client_id = msgObj.client_id
};

HelpDescAdmin.prototype.updateConversation = function(msgObj){
    var conversationHTML = this.statusItemTmpl(msgObj),
        $clientItem = this.$clientsList.find(
        '.conversation-item[data-client-id="' + msgObj.client_id + '"]'
    );
    if($clientItem.length){
        $clientItem.replaceWith(conversationHTML)
    } else {
        this.$clientsList.append(conversationHTML)
    }
};

HelpDescAdmin.prototype.addMessage = function(msgObj){
    var createdAtDate = new Date(msgObj.created_at * 1000);
    msgObj.user_name = msgObj.user_name || 'Anonymous';
    msgObj.created_at = createdAtDate.toLocaleString();
    this.$output.append(this.msgCompiledTmpl(msgObj));
};

HelpDescAdmin.prototype.getConnection = function(restore){
    var self = this;
    if(!this.ws || restore){
        this.ws = new WebSocket(appConfig.wsUrl);
        this.ws.onmessage = function(wsMessage){
            self.onMessage(wsMessage)
        };
        this.ws.onclose = function(){
            self.onClose()
        };
    }
    return this.ws
};

HelpDescAdmin.prototype.post = function(msgObj){
    this.getConnection().send(JSON.stringify(msgObj))
    this.addMessage(msgObj)
};

HelpDescAdmin.prototype.init = function(){
    this.bindings();
    this.getConnection();
};

HelpDescAdmin.prototype.loadMessages = function(clientID, callback){
    $.ajax({url: '/admin/messages/' + clientID}).done(function(data){
        if(callback){
            callback(data)
        }
    }).error(function(er){
        alert(er)
    });
};

HelpDescAdmin.prototype.bindings = function(){
    var self = this;
    if(!HelpDescAdmin.isBinded){

        $(document).on('click', '#conversationsList .btn', function(){
            var $btn = $(this),
                clientID = $btn.closest('.conversation-item').data('client-id');

            $btn.button('loading');
            self.loadMessages(clientID, function(data){
                var $modal = $($btn.data('target'));

                $modal.data('client-id', clientID);
                self.$output.html('');

                $.each(data.messages, function(index, msgObj){
                    self.addMessage(msgObj)
                });

                $modal.modal('show');
                $btn.button('reset');
            })
        });

        $(document).on('click', '#conversationModal .btn', function(){
            var $modal = $('#conversationModal'),
                $input = $('#messageInput'),
                clientID = $modal.data('client-id'),
                msgObj = {
                    user_name: 'Admin',
                    client_id: clientID,
                    message: $input.val(),
                    created_at: new Date().getTime() / 1000
                };
            self.post(msgObj);
            $input.val('');
        });
    }
    HelpDescAdmin.isBinded = true

};

$(document).on('ready', function(){
    var admin = new HelpDescAdmin();
    admin.init();
});